FROM cloudron/base:0.8.0
MAINTAINER Johannes Zellner <support@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

EXPOSE 8000

#### WARNING
# The palava_machine gem does not yet support setting REDIS url https://github.com/palavatv/palava-machine/pull/6
RUN gem install bundler palava_machine middleman

RUN apt-get update
RUN apt-get install -y redis-server

# RUN curl -L https://github.com/palavatv/palava-portal/archive/master.tar.gz | tar -xz --strip-components 1 -f -
RUN git clone https://github.com/palavatv/palava-portal . && git submodule init && git submodule update

RUN bundle --without development:test --deployment

## inject placeholder for sed
RUN PALAVA_RTC_ADDRESS="##PALAVA_RTC_ADDRESS##" PALAVA_STUN_ADDRESS="##PALAVA_STUN_ADDRESS##" PALAVA_BASE_ADDRESS="##PALAVA_BASE_ADDRESS##" bundle exec middleman build

# stash as template
RUN cp -rf build/ /app/code/build_template
RUN rm -rf /app/code/build && ln -s /run/build /app/code/build

ADD supervisor/ /etc/supervisor/conf.d/
ADD nginx-palava /etc/nginx/sites-enabled/palava

ADD start.sh /app/code/start.sh
RUN chmod +x /app/code/start.sh

CMD [ "/app/code/start.sh" ]
