#!/bin/bash

set -eux

# inject our HOSTNAME related vars into the static assets
rm -rf /run/build
cp -rf /app/code/build_template /run/build
find /app/code/build/ -type f | xargs sed -i \
	-e "s/##PALAVA_RTC_ADDRESS##/wss:\/\/${HOSTNAME}\/info\/machine/" \
   	-e "s/##PALAVA_STUN_ADDRESS##/stun:stun.palava.tv/" \
   	-e "s/##PALAVA_BASE_ADDRESS##/https:\/\/${HOSTNAME}/"

/usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i PALAVA
