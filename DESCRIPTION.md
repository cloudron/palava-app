
Palava.tv enables video communication with your friends and colleagues from within your web browser! No registration is required and you don't need to install any browser plugins!

### How to use palava?

You can join or initiate a video conference by simply entering a name for it on the start page and pressing <Enter>. If nobody is in the room, yet, a new one will be automatically created. Your browser will then ask you for access to your microphone and webcam. Pay attention to not overlook it, for example, in Chrome, it is just a small bar at the top. In Firefox, it might take a minute, when joining for the first time. After accepting, you have successfully joined/created the conference and you can start to invite others by sharing your current internet address! Keep it mind that your participants also need a compatible browser:

 * Google Chrome 26 or above
 * Mozilla Firefox 22 or above
 * Windows Opera 18 or above

It is possible to create hidden rooms by clicking the <Private> button on the start page. It will create a long random name for the conference, which noone can guess. Share this link as usual to invite you friends. Only people who know this exact link can join!

### How does it work under the hood?

palava.tv is based on a new technology called Web Real-Time Communication (WebRTC), which enables pluginless video conferencing via peer-to-peer connections. Please see our blog or our github page for more technical information!
