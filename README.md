# Palava Cloudron App

This repository contains the Cloudron app package source for [Palava](https://github.com/palavatv).

## Installation

[![Install](https://cloudron.io/img/button32.png)](https://cloudron.io/button.html?app=tv.palava.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id tv.palava.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd palava-app
cloudron build
cloudron install
```
